public class wrapperQuestions
{
    public string             questions {get;set;}
    public string             options{get;set;}
    public string             type{get;set;}
    public date               dateInput{get;set;}
    public integer            quesNumber {get;set;}
    public list<selectoption> optionList{get;set;} 
    public boolean            selectRadio1 { get; set; }
    public String             textAns { get; set; }
    public list<string>       selectValueCheckbox{get;set;}
    public wrapperQuestions()
    {
        quesNumber = 0;
        type = '';
        options='';
        questions ='';
        textAns ='';
        dateInput = Date.today();
        selectValueCheckbox = new List<String>();
        selectRadio1 = false;
        optionList=new list<selectoption>();
        
    }
    
    public void populateOptionList()
    {
        if((this.type=='Multi-Select'||this.type=='Checkbox'||this.type=='Radio')&& this.options!=NULL)
        {
            for(string s : this.options.split(';'))
            {
                if(this.type=='Radio')
                {
                    if(s=='Yes')
                    {
                        this.optionList.add(new selectoption('true',s));            
                    }
                    else if(s=='No')
                    {
                        this.optionList.add(new selectoption('false',s));
                    }
                }
                else
                {
                    this.optionList.add(new selectoption(s,s));
                }
            }            
        }
    }   
}